#pragma once
#include <vector>

#include "SDL.h"

// set to 0 to skip generation
#define DRAW_GENERATION 0

struct MazeTile
{
	bool leftWall = true;
	bool upWall = true;

	// generation variables
	bool visitedGen = false;

	// solving variables
	bool visitedSolv = false;
	bool onStackSolv = false;
	bool first = false;
	bool last = false;
	bool path = false;

	// coloring variables
	bool onStackGen = false;
	bool current = false;
};

class Maze
{
public:
	Maze(int x, int y, int tileSize = 10);

	// generation algorithms

	// Generates a maze using DFS method to visit every tile
	void RecursiveBacktracker();
	// Generates a maze by randomly removing up or left wall of each tile
	void BinaryTree();
	// Generates a maze by connecting tiles into sets
	void EllersAlgorithm();
	// Generates a maze using minimum spanning tree
	void KruskalsAlgorithm();
	// Generates a maze by recursivly dividing a space with a wall and adding a hole in it
	void RecursiveDivision();
	// Generates a maze by randomly walking over cells
	void AldousBroderAlgorithm();

	// solving algorithms

	void BreadthFirstSearch();
	void DepthFirstSearch();
	void AStar();

	// draws current maze
	void Draw();

private:

	void ResetMazeTiles();

	// helper function for Eller's algorightm
	void JoinTilesHorizontally(std::vector<int>& row, int rowIndex);
	// helper function for Eller's algorightm
	void JoinTilesVertically(std::vector<int>& row, int rowIndex);

	// helper function for RecursiveDivision algorithm
	void Split(int startPrimary, int endPrimary, int startSecundary, int endSecundary, bool up);

	// helper function for AStar
	int GridDistance(int a, int b);


	// helper function for Eller's algorithm and binary tree visualization
	void DrawGenerationStep(int tileIndex);

private:
	// number of tiles on each axis
	int x, y;
	// list of maze tiles
	std::vector<MazeTile> tiles;

	// rendering variables
	SDL_Window* window;
	SDL_Renderer* renderer;

	int tileSize;
};