#include <iostream>
#include <time.h>

#include "maze.h"

int main(int argc, char* argv[])
{
	srand(time(NULL));
	Maze maze(10, 10, 20);
	maze.Draw();
	SDL_Delay(3);
	maze.KruskalsAlgorithm();
	maze.Draw();
	SDL_Delay(3000);
	maze.AStar();
	SDL_Delay(200);
	return 0;
}