#include <stack>
#include <queue>
#include <unordered_map>
#include <list>
#include <random>
#include <algorithm>
#include <iostream>

#include "maze.h"

Maze::Maze(int x, int y, int tileSize)
	:x(x), y(y), tiles(x* y, MazeTile()), tileSize(tileSize)
{
	// create SDL window
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, x * tileSize, y * tileSize, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, 0);
}

void Maze::RecursiveBacktracker()
{
	// setup stack
	std::stack<int> stack;
	stack.push(0);
	tiles[0].visitedGen = true;

#if DRAW_GENERATION
	tiles[0].onStackGen = true;
	tiles[0].current = true;
#endif

	while (!stack.empty())
	{
		// get element on top of stack
		int curr = stack.top();

		// find adjacent tiles that are not visited
		std::vector<int> adjacent;

		// check left
		if (curr % x != 0 && !tiles[curr - 1].visitedGen)
			adjacent.push_back(curr - 1);

		// check right
		if((curr + 1) % x != 0 && !tiles[curr + 1].visitedGen)
			adjacent.push_back(curr + 1);

		// check up
		if(curr >= x && !tiles[curr - x].visitedGen)
			adjacent.push_back(curr - x);

		// check down
		if(curr + x < tiles.size() && !tiles[curr + x].visitedGen)
			adjacent.push_back(curr + x);

		// if no adjecent tiles pop current from stack
		// else add one of adjacent tiles to the stack
		if (adjacent.size() == 0)
		{
			stack.pop();

#if DRAW_GENERATION
			// remove last tile from stack
			tiles[curr].onStackGen = false;
			// mark previous tile as current
			if(stack.size() > 0)
				tiles[stack.top()].current = true;
#endif
		}
		else
		{
			// chose next tile randomly and add it to the stack
			int next = adjacent[rand() % adjacent.size()];
			tiles[next].visitedGen = true;

			stack.push(next);

			// remove wall
			// went left
			if (curr - 1 == next)
				tiles[curr].leftWall = false;
			// went right
			else if (curr + 1 == next)
				tiles[next].leftWall = false;
			// went up
			else if (curr - x == next)
				tiles[curr].upWall = false;
			// went down
			else if (curr + x == next)
				tiles[next].upWall = false;

#if DRAW_GENERATION
			// add new tile to stack and mark it as current
			tiles[next].onStackGen = true;
			tiles[next].current = true;
#endif
		}

#if DRAW_GENERATION
		// draw each step of the generation
		tiles[curr].current = false;
		Draw();
		SDL_Delay(50);
#endif
	}
}

void Maze::BinaryTree()
{
	// loop over every tile from last to first
	for (int i = tiles.size() - 1; i >= 0; i--)
	{
		// if upper left tile remove nothing
		if (i == 0) {}
		// if tile is in first row remove left wall
		else if (i < x)
		{
			tiles[i].leftWall = false;
		}
		// if tile is in first column remove up wall
		else if (i % x == 0)
		{
			tiles[i].upWall = false;
		}
		// else remove random wall
		else
		{
			if (rand() % 2 == 0)
				tiles[i].leftWall = false;
			else
				tiles[i].upWall = false;
		}

#if DRAW_GENERATION
		// draw each step of the generation
		DrawGenerationStep(i);
#endif
	}
}

void Maze::EllersAlgorithm()
{
	std::vector<int> row(x, 0);

	int largestIndex = 0;
	
	for (size_t j = 0; j < y; j++)
	{
		for (size_t i = 0; i < x; i++)
		{
			if (row[i] == 0)
				row[i] = ++largestIndex;
		}

		JoinTilesHorizontally(row, j);
		JoinTilesVertically(row, j);
	}
}

// Helper function for Eller's algorithm
// Joins tiles that are not in the same set randomly
void Maze::JoinTilesHorizontally(std::vector<int>& row, int rowIndex)
{	
	// loop over current row
	for (int i = x-1; i >= 0; i--)
	{
		// randomly connect if not last row
		bool connect = rand() % 2 == 0;
		if (rowIndex == y - 1)
			connect = true;

		int tileIndex = rowIndex * x + i;
		// if tiles are not in the same set join randomly
		if (i != 0 && row[i] != row[i-1] && connect)
		{
			// remove wall between joined sets
			tiles[tileIndex].leftWall = false;

			// change all conected tiles to same set
			int currSet = row[i];
			int newSet = row[i - 1];
			for (int u = 0; u < x; u++)
			{
				if (row[u] == currSet)
					row[u] = newSet;
			}
		}

#if DRAW_GENERATION
		DrawGenerationStep(tileIndex);
#endif
	}
}

// Helper function for Eller's algorithm
// Joins at least one tile from every set with tile below
void Maze::JoinTilesVertically(std::vector<int>& row, int rowIndex)
{
	if (rowIndex == y - 1)
		return;

	std::unordered_map<int, std::vector<int>> umap;
	// loop over current row
	// create unordered map of all sets
	for (int i = 0; i < x; i++)
	{
		int tileIndex = rowIndex * x + i;
		// populate unordered map with indices of tiles in set
		// key = set, value = indices of all tiles in this set
		if (umap.find(row[i]) == umap.end())
		{
			// if no current set create new vector
			umap[row[i]] = std::vector<int>(1, tileIndex);
		}
		else
		{
			// if set exists add index to vector
			umap.find(row[i])->second.push_back(tileIndex);
		}
		
		row[i] = 0;
	}

	// loop every set
	for (auto itr = umap.begin(); itr != umap.end(); itr++) 
	{
		// connect every set to bottom row at least once
		int numberOfConnections = rand() % itr->second.size() + 1;
		int tileIndex;
		while (numberOfConnections > 0)
		{
			tileIndex = itr->second[ rand() % itr->second.size()];
			row[tileIndex % x] = itr->first;
			tiles[tileIndex + x].upWall = false;
			numberOfConnections--;
		}

#if DRAW_GENERATION
		DrawGenerationStep(tileIndex + x);
#endif

	} 
}

void Maze::KruskalsAlgorithm()
{
	// Algorithm used to create minimum spanning trees

	// Chose wall randomy and remove it tiles are not in the same set

	// Represent walls as edges and tiles as nodes
	// Nodes in the same tree are tiles in same set

	// In separate algorithm tiles should be initialized as nodes to skip expensive steps
	
	// start of conversion
	// Create a list of all nodes (tiles)
	struct Node
	{
		int tileIndex = 0;
		Node* parent = nullptr;
		// children in tree are not important so we don't record them

		// return index of root node
		int RootNode()
		{
			if (parent == nullptr)
				return tileIndex;
			return parent->RootNode();
		}
	};

	std::vector<Node> nodeList;
	nodeList.reserve(tiles.size());
	
	// Create a list of all edges (walls)
	struct Edge
	{
		int tileNumber = 0;
		bool upWall = false;
	};

	std::vector<Edge> edgeList;
	edgeList.reserve(tiles.size() * 2 - 2 * x);

	// add nodes and edges to the list
	for (int i = 0; i < tiles.size(); i++)
	{
		// add each node (tile) to node list
		nodeList.push_back({ i, nullptr });

		// add each edge (inner walls) to the list
		if (i % x != 0) // skip if left column
			// add left wall
			edgeList.push_back({ i, false });
		if(i >= x) // skip if top row
			// add up wall
			edgeList.push_back({ i, true });
	}
	// end of conversion


	// randomize edge order
	std::random_shuffle(std::begin(edgeList), std::end(edgeList));

	int i = 0;
	// loop over every edge (inner wall)
	// if edge is not connecting nodes in the same tree (tiles not in same set) remo it
	for (auto& edge : edgeList)
	{
		// indices of two tiles connected by current edge
		int currOne = edge.tileNumber;
		int currTwo = (edge.upWall) ? edge.tileNumber - x : edge.tileNumber - 1;

		// if root nodes of current nodes is not the same connect them
		if (nodeList[currOne].RootNode() != nodeList[currTwo].RootNode())
		{
			// connect them to same tree
			Node* root = &nodeList[currTwo];
			// find root of the tree
			while (root->parent != nullptr) root = root->parent;
			// add root to current node
			root->parent = &nodeList[currOne];

			// remove the wall
			if(edge.upWall)
				tiles[currOne].upWall = false;
			else
				tiles[currOne].leftWall = false;

			// mark them as visited
			tiles[currOne].visitedGen = true;
			tiles[currTwo].visitedGen = true;
		}

#if DRAW_GENERATION
		// mark current tiles
		tiles[currOne].current = true;
		tiles[currTwo].current = true;

		Draw();
		SDL_Delay(50);

		tiles[currOne].current = false;
		tiles[currTwo].current = false;
#endif

	}

}

void Maze::RecursiveDivision()
{
	// Start with emty space
	// Recursively devide the space with a wall and add a hole in that wall

	// Remove all walls to start with empty space
	for (auto& tile : tiles)
	{
		tile.upWall = false;
		tile.leftWall = false;
		tile.visitedGen = true;
	}

	// Start recursive division
	Split(0, x, 0, y, true);
}

void Maze::Split(int startPrimary, int endPrimary, int startSecundary, int endSecundary, bool xAxis)
{
	// primary -> current axis we are dividing
	// secundary -> axis along wich wall will be created

	if (startPrimary == endPrimary - 1)
		return;

	// position where to split current axis
	// random between start and end, excluding start and end
	int splitPrimary = (rand() % (endPrimary - startPrimary - 1)) + 1 + startPrimary;
	// position where to add hole in the wall
	// random between start and end, excluding end
	int splitSecundary = (rand() % (endSecundary - startSecundary)) + startSecundary;

	// loop over every space and add wall
	for (int i = startSecundary; i < endSecundary; i++)
	{
		// skip if hole in current position
		if (i != splitSecundary)
		{
			// if current axis is X add left walls
			if (xAxis)
				tiles[splitPrimary + i * x].leftWall = true;
			// otherwise add up walls
			else 
				tiles[splitPrimary * x + i].upWall = true;

		}
	}

#if DRAW_GENERATION
	Draw();
	SDL_Delay(50);
#endif

	// change primary and secundary axis
	// recursively work on both sides of the split
	Split(startSecundary, endSecundary, startPrimary, splitPrimary, !xAxis);
	Split(startSecundary, endSecundary, splitPrimary, endPrimary, !xAxis);
}

void Maze::AldousBroderAlgorithm()
{

	// chose random starting position and mark it as visited
	int current = rand() % tiles.size();
	tiles[current].visitedGen = true;

	// keep trak of unique cells visited
	int cellsVisited = 1;

	// loop until every cell is visited
	while (cellsVisited < tiles.size())
	{
		int next = 0;
		int rnd = 0;

		// find walid move
		bool falseMove = true;
		while (falseMove)
		{
			// chose one of the 4 direction and check if it is possible
			rnd = rand() % 4;

			switch (rnd)
			{
			case 0: // up
				if (current > x) 
				{
					falseMove = false;
					next = current - x;
				}
				break;
			case 1: // right
				if (current % x < x - 1) 
				{
					falseMove = false;
					next = current + 1;
				}
				break;
			case 2: // down
				if (current < (x-1) * y) 
				{
					falseMove = false;
					next = current + x;
				}
				break;
			case 3: // left
				if (current % x > 0) 
				{
					falseMove = false;
					next = current - 1;
				}
				break;
			default:
				break;
			}
		}

#if DRAW_GENERATION
		tiles[current].current = true;
		Draw();
		SDL_Delay(50);
		tiles[current].current = false;
#endif

		// if next cell to visit was not already visited
		// mark it as visited and remove the wall
		if (!tiles[next].visitedGen)
		{
			tiles[next].visitedGen = true;
			// increment number of unique cell visits
			cellsVisited++;

			switch (rnd)
			{
			case 0: // up
				tiles[current].upWall = false;
				break;
			case 1: // right
				tiles[next].leftWall = false;
				break;
			case 2: // down
				tiles[next].upWall = false;
				break;
			case 3: // left
				tiles[current].leftWall = false;
				break;
			default:
				break;
			}
		}

		current = next;
	}
}

void Maze::BreadthFirstSearch()
{
	// chose start and end tiles
	tiles[0].first = true;
	tiles[tiles.size() - 1].last = true;

	// create tree structure to recreate final path
	struct Node
	{
		int tileIndex = 0;
		Node* parent = nullptr;
		// no need to keep track of node children

		Node(int i, Node* p) :tileIndex(i), parent(p) {}
	};

	Node root{ 0, nullptr };

	// using queue datastructure for FIFO
	std::queue<Node*> q;
	q.push(&root);

	int current = 0;
	// loop until end tile is found
	while (current != tiles.size() - 1)
	{
		// add each possible next step to queue
		// add up
		if (current >= x && !tiles[current].upWall && !tiles[current - x].visitedSolv)
		{
			// add new tile to the tree, with current tile as a parent node
			q.push(new Node(current - x, q.front()));
			tiles[current - x].onStackSolv = true;
			tiles[current - x].visitedSolv = true;
		}
		// add right
		if (current % x != x - 1 && !tiles[current + 1].leftWall && !tiles[current + 1].visitedSolv) 
		{
			q.push(new Node(current + 1, q.front()));
			tiles[current + 1].onStackSolv = true;
			tiles[current + 1].visitedSolv = true;
		}
		// add down
		if (current < (x - 1) * y && !tiles[current + x].upWall && !tiles[current + x].visitedSolv)
		{
			q.push(new Node(current + x, q.front()));
			tiles[current + x].onStackSolv = true;
			tiles[current + x].visitedSolv = true;
		}

		// add left
		if (current % x != 0 && !tiles[current].leftWall && !tiles[current - 1].visitedSolv) 
		{
			q.push(new Node(current - 1, q.front()));
			tiles[current - 1].onStackSolv = true;
			tiles[current - 1].visitedSolv = true;
		}

		// remove current tile from stack
		q.pop();
		tiles[current].onStackSolv = false;
		current = q.front()->tileIndex;

		Draw();
		SDL_Delay(50);
	}

	// traverse up the created tree to find the shortest path
	Node* temp = q.front();
	while (temp != nullptr)
	{
		tiles[temp->tileIndex].path = true;
		temp = temp->parent;
		Draw();
		SDL_Delay(50);
	}
}

void Maze::DepthFirstSearch()
{
	// chose start and end tiles
	tiles[0].first = true;
	tiles[tiles.size() - 1].last = true;

	std::stack<int> stack;

	Draw();
	SDL_Delay(50);

	stack.push(0);

	tiles[0].visitedSolv = true;

	while (stack.top() != tiles.size() - 1)
	{
		int current = stack.top();

		int numOfActions = 0;
		int possibleActions[4]{ 0,0,0,0 };


		// find all possible moves
		// add up
		if (current >= x && !tiles[current].upWall && !tiles[current - x].visitedSolv && !tiles[current - x].onStackSolv)
		{
			// add action if possible
			possibleActions[numOfActions] = current - x;
			numOfActions++;
		}
		// add right
		if (current % x != x - 1 && !tiles[current + 1].leftWall && !tiles[current + 1].visitedSolv && !tiles[current + 1].onStackSolv) 
		{
			possibleActions[numOfActions] = current + 1;
			numOfActions++;
		}
		// add down
		if (current < (x - 1) * y && !tiles[current + x].upWall && !tiles[current + x].visitedSolv && !tiles[current + x].onStackSolv)
		{
			possibleActions[numOfActions] = current + x;
			numOfActions++;
		}

		// add left
		if (current % x != 0 && !tiles[current].leftWall && !tiles[current - 1].visitedSolv && !tiles[current - 1].onStackSolv) 
		{
			possibleActions[numOfActions] = current - 1;
			numOfActions++;
		}

		if (numOfActions > 0)
		{
			int action = possibleActions[rand() % numOfActions];
			stack.push(action);
			tiles[action].onStackSolv = true;
		}
		else
		{
			tiles[current].onStackSolv = false;
			tiles[current].visitedSolv = true;
			stack.pop();
		}

		Draw();
		SDL_Delay(50);

	}

	while (stack.size() > 0)
	{
		tiles[stack.top()].path = true;
		stack.pop();
		Draw();
		SDL_Delay(50);
	}

}

void Maze::AStar()
{
	// chose start and end tiles
	tiles[0].first = true;
	tiles[tiles.size() - 1].last = true;

	// create tree structure to recreate final path
	struct Node
	{
		int index;
		int local; // steps to get to this point
		int global; // estimated steps to completion
		
		Node* parent; // follow parent nodes to get to closest path from origin
		Node(int i, int l, int g, Node* p) :index(i), local(l), global(g), parent(p) {}
	};

	// create node for first tile and mark it as visited
	int current = 0;
	tiles[0].visitedSolv = true;
	// store all nodes to check later
	std::vector<Node> nodes(x * y, Node(0, 0, 0, nullptr));
	nodes[0].global = GridDistance(0, tiles.size() - 1);

	// add first node to the list
	std::list<Node*> list;
	list.push_back(&nodes[0]);

	// loop until end tile is found
	// will not always find the shortest path
	Node* currentNode = &nodes[0];
	while (current != tiles.size() - 1) 
	{
		// check all possible moves
		// loop for every possible action
		for (int j = 0; j < 4; j++)
		{
			// calculate tile index and if move is valid for each of 4 possible directions
			int i = 0;
			bool validMove = false;
			if (j == 0) // up move
			{
				i = current - x; // calculate tile index of resulting move
				validMove = current >= x && !tiles[current].upWall; // check if move is valid
			}
			else if (j == 1) // right move
			{
				i = current + 1;
				validMove = current % x != x - 1 && !tiles[current + 1].leftWall;
			}
			else if (j == 2) // down move
			{
				i = current + x;
				validMove = current < (x - 1) * y && !tiles[current + x].upWall;
			}
			else if (j == 3) // left move
			{
				i = current - 1;
				validMove = current % x != 0 && !tiles[current].leftWall;
			}

			// repeat the same action for every possible move
			if (validMove)
			{
				if (!tiles[i].visitedSolv)
				{
					// if node is visited first time calculate local and global values
					nodes[i].index = i;
					nodes[i].local = currentNode->local + 1;
					nodes[i].global = currentNode->local + 1 + GridDistance(i, tiles.size() - 1);
					nodes[i].parent = currentNode;
					// add node to list and makr it as visited
					list.push_back(&nodes[i]);
					tiles[i].onStackSolv = true;
				}
				else
				{
					// if node vas already visited update values if current path is better than previous
					Node* node = &nodes[i];

					if (node->local > currentNode->local + 1)
					{
						node->parent = currentNode;
						node->local = currentNode->local + 1;
						node->global = currentNode->local + 1 + GridDistance(current - x, tiles.size() - 1);
					}
				}
			}
		}

		// sort list to find node with smalles global value
		list.sort([](const Node* lhs, const Node* rhs) { return lhs->global < rhs->global; });
		// mark node with smalles global as current and remove it from the list
		currentNode = list.front();
		list.pop_front();
		current = currentNode->index;
		// mark tile as visited
		tiles[current].onStackSolv = false;
		tiles[current].visitedSolv = true;

		Draw();
		SDL_Delay(50);
	}

	// traverse up the tree and mark path along the way
	while (currentNode->index != 0)
	{
		tiles[currentNode->index].path = true;
		currentNode = currentNode->parent;
		 
		Draw();
		SDL_Delay(50);
	}

}

// calculate grid distance from tile index
int Maze::GridDistance(int a, int b)
{
	int xLength = std::abs(a % x - b % x);
	int yLength = std::abs(a / x - b / x);

	return std::round(std::sqrt(xLength * xLength + yLength * yLength));
}

// helper function for Eller's algorithm and binary tree visualization
void Maze::DrawGenerationStep(int tileIndex)
{
	// draw each step of the process
	tiles[tileIndex].visitedGen = true;
	tiles[tileIndex].current = true;
	Draw();
	tiles[tileIndex].current = false;

	SDL_Delay(50);
}



// draw maze to screen
void Maze::Draw()
{
	// clear screen
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);

	SDL_Rect rect;
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			// set tile size
			rect.w = tileSize;
			rect.h = tileSize;
			// set current tile position
			rect.x = i * tileSize;
			rect.y = j * tileSize;

#if DRAW_GENERATION
			// draw tiles in different colors
			// generating colors
			if(tiles[i + j * x].current == true)
				SDL_SetRenderDrawColor(renderer, 0, 153, 0, 255);
			else if(tiles[i + j * x].onStackGen == true)
				SDL_SetRenderDrawColor(renderer, 150, 150, 150, 255);
			else if(tiles[i + j * x].visitedGen == true)
				SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
			// solving colors
			else if(tiles[i + j * x].visitedSolv == true)
				SDL_SetRenderDrawColor(renderer, 0, 153, 153, 255);
			else if(tiles[i + j * x].onStackSolv == true)
				SDL_SetRenderDrawColor(renderer, 153, 0, 0, 255);
			else if(tiles[i + j * x].first == true)
				SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
			else if(tiles[i + j * x].last == true)
				SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
			else
				SDL_SetRenderDrawColor(renderer, 30, 30, 30, 255);
#else
			// draw only white tiles
			if(tiles[i + j * x].first == true)
			SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
			else if(tiles[i + j * x].last == true)
			SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
			else if(tiles[i + j * x].path == true)
				SDL_SetRenderDrawColor(renderer, 0, 155, 0, 255);
			else if(tiles[i + j * x].onStackSolv == true)
				SDL_SetRenderDrawColor(renderer, 255, 100, 100, 255);
			else if(tiles[i + j * x].visitedSolv == true)
					SDL_SetRenderDrawColor(renderer, 55, 200, 200, 255);
			else
				SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
#endif
			SDL_RenderFillRect(renderer, &rect);

			// draw tile edges
			// draw vertical edge (left)
			if (tiles[i + j * x].leftWall)
			{
				rect.w = 1;
				rect.h = tileSize;
				SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
				SDL_RenderFillRect(renderer, &rect);
			}

			// draw horizontal edge (upper)
			if (tiles[i + j * x].upWall)
			{
				rect.w = tileSize;
				rect.h = 1;
				SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
				SDL_RenderFillRect(renderer, &rect);
			}
		}
	}

	// draw edge walls
	rect.w = tileSize * x;
	rect.h = tileSize * y;
	rect.x = 0;
	rect.y = 0;
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderDrawRect(renderer, &rect);

	// show render
	SDL_RenderPresent(renderer);
}