## Maze generation and pathfinding algorithms

[[_TOC_]]

## Maze Generation

### Recursive Backtracking

Recursive Backtracking uses DFS to visit every tile in a maze, removing walls along the way.

![Recursive Backtracking](Mazes/demo/recursive_backtracker.gif)

### Binary Tree

Binary Tree algorithm visits every cell and chooses whether to remove horizontal or vertical wall.\
This creates a bias on edge cases where only one of the walls can be removed and that creates empty corridors.

![Binary Tree](Mazes/demo/binary_tree.gif)

### Eller's Algorithm

Eller's Algorithm generates maze row by row. Tiles are randomly connected into bigger sets.\
Each set is expanded at least once to the next row by connecting to a random cell.\
Cells that were not expanded into are given new sets and algorithm repeats.\
On the last row all sets are connected.

![Eller's Algorithm](Mazes/demo/ellers_algorithm.gif)

### Kruskal's Algorithm

Kruskalís algorithm is a method for producing a minimal spanning tree from a weighted graph.\
Walls are represented as edges and tiles as nodes. It generates a maze by randomly chosing a wall\
and removing it if two nodes are not in the same tree.

![Kruskal's Algorithm](Mazes/demo/kruskals_algorithm.gif)

### Recursive Division

Recursive division starts with an empty space and devides it up with a wall that has a hole in it.\
Step is repeated on both sides of the split but on the other axis.\
This algorithm can repeat indefinitely at progressively finer and finer levels of detail.

![Recursive Division](Mazes/demo/recursive_division.gif)

### Aldous-Broder Algorithm

One of the worst and slowest algorithms for maze generation. Usually used for graphs.\
Works like Recursive backtracking without checking if next cell to visit was already visited.

![Aldous-Broder Algorithm](Mazes/demo/aldous_broder_algorithm.gif)

## Maze Solving

### Breadth-first search 

Simple BFS method of finding the shortest path.

![Breadth-first search](Mazes/demo/breadth_first_search.gif)

### Depth-first search 

Simple DFS method of finding the shortest path. Since there are no loops in the maze it will find the shortest path.

![Depth-first search](Mazes/demo/depth_first_search.gif)

### A* search algorithm 

A* algorithm is graph traversal algorithm. A* achieves better performance by using heuristics to guide its search.\
It is a best-first search algorithm.

![AStar algorithm](Mazes/demo/astar.gif)